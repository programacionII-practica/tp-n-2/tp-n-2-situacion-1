/*
Situación 1
Se necesita realizar una representación de objetos de tipo alimento. De los mismos se conocen su
descripción, el contenido de lípidos, el contenido de hidratos de carbono y el contenido de proteínas expresado en porcentajes, si es o no de origen animal, el contenido de vitaminas representado por A (Alto), M (medio) y B (Bajo).
Adicionalmente se pide definir otra clase que permita instanciar objetos de la clase alimento y
mostrar la información del alimento, indicar si un alimento es dietético, lo cual sucede cuando el
alimento contiene menos del 20 % de grasas y el contenido de vitamina no en bajo; si es recomendable para deportistas lo cual es cierto si el alimento contiene lo siguiente: proteínas: 10-
15%; hidratos de carbono:55-65% y grasas 30-35%; e informar el valor energético del alimento.
Teniendo en cuenta que este valor resulta de aplicar la siguiente formula:
lípidos * 9,4 +hidratos * 4,1 +proteínas * 5,3.
*/
package situacion;

import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest 
    {
    @Test 
        public void Informacion()
        {
            Alimento nuevoAlimento = new Alimento();
            nuevoAlimento.setInformacion("Banana","Es un fruto comestible, de varios tipos de grandes plantas herbaceas del genero Musa");
            
            

            assertEquals("Banana",nuevoAlimento.getNombre());
            assertEquals("Es un fruto comestible, de varios tipos de grandes plantas herbaceas del genero Musa",nuevoAlimento.getDescripcion());

        }
    @Test
        public void ContenidoPorcentaje()
        {
            Alimento nuevoAlimento = new Alimento();
            nuevoAlimento.setContenidoPorcentaje(13,56,31);
            assertEquals(13,nuevoAlimento.getContenidoGrasas());
            assertEquals(56,nuevoAlimento.getContenidoHidratos());
            assertEquals(31,nuevoAlimento.getContenidoProteinas());
        }
    @Test
        public void ContenidoVitaminas()
        {
            Alimento nuevoAlimento = new Alimento();
            nuevoAlimento.setContenidoVitaminas('B');
            assertEquals('B',nuevoAlimento.getContenidoVitaminas());
        }
    @Test
        public void OrigenAnimal()
        {
            Alimento nuevoAlimento = new Alimento();
            nuevoAlimento.setOrigenAnimal(true);
            assertTrue(nuevoAlimento.getOrigenAnimal());
        }
    @Test
        public void MostrarInformacion()
        {
            Alimento nuevoAlimento = new Alimento();
            nuevoAlimento.setInformacion("Banana","Es un fruto comestible, de varios tipos de grandes plantas herbaceas del genero Musa");
            nuevoAlimento.setContenidoVitaminas('B');
            nuevoAlimento.setContenidoPorcentaje(80,40,20);
            nuevoAlimento.setOrigenAnimal(false);

            InfoAlimento infoAlimento = new InfoAlimento();
            infoAlimento.mostrarInformacion(nuevoAlimento);
        }
    @Test
        public void esDieteticoTrue()   // Debe retornar True
        {
            Alimento nuevoAlimento = new Alimento();
            InfoAlimento nuevaInfoAlimento = new InfoAlimento();

            nuevoAlimento.setContenidoPorcentaje(15,30,20);
            nuevoAlimento.setContenidoVitaminas('M');
            assertTrue(nuevaInfoAlimento.esDietetico(nuevoAlimento));
        }
    @Test
        public void esDieteticoFalse() // Debe retornar False
        {
            Alimento nuevoAlimento = new Alimento();
            InfoAlimento nuevaInfoAlimento = new InfoAlimento();

            nuevoAlimento.setContenidoPorcentaje(30,30,20);
            nuevoAlimento.setContenidoVitaminas('B');
            assertFalse(nuevaInfoAlimento.esDietetico(nuevoAlimento));
        }
    @Test
        public void esRecomendableDeportistasFalse()
        {
            Alimento nuevoAlimento = new Alimento();
            InfoAlimento nuevaInfoAlimento = new InfoAlimento();

            nuevoAlimento.setContenidoPorcentaje(9,30,20);
            assertFalse(nuevaInfoAlimento.esRecomendableDeportistas(nuevoAlimento));
        }
    @Test
        public void esRecomendableDeportistasTrue()
        {
            Alimento nuevoAlimento = new Alimento();
            InfoAlimento nuevaInfoAlimento = new InfoAlimento();

            nuevoAlimento.setContenidoPorcentaje(12,60,35);
            assertTrue(nuevaInfoAlimento.esRecomendableDeportistas(nuevoAlimento));
        }
    @Test
        public void valorEnergetico() // Con los valores de contenido especificados, se realiza el calculo
        {                             // del valor energetico por separado (mediante una calculadora),
            Alimento nuevoAlimento = new Alimento();    // y se busca que el metodo retorne el mismo valor
            InfoAlimento nuevaInfoAlimento = new InfoAlimento();

            nuevoAlimento.setContenidoPorcentaje(10,60,30);
            assertEquals((int) 499, (int) nuevaInfoAlimento.valorEnergetico(nuevoAlimento));
        }   // Se utiliza "casting" para que ambos valores sean de tipo Integer
    }       // Esto es con el fin de facilitar la comparacion entre ambos valores. Ya que,
            // es más dificil comparar con precision dos valores decimales.
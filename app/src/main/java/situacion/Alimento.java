package situacion;

public class Alimento {
    // Atributos
    private String nombre;
    private String descripcion;
    private char contenidoVitaminas;
    private Integer contenidoGrasas;
    private Integer contenidoHidratos;
    private Integer contenidoProteinas;
    private boolean origenAnimal;

    // Metodos "setter". Permiten inicializar las variables
    public void setInformacion(String nombre, String descripcion)
    {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
    public void setContenidoVitaminas(char contenidoVitaminas)
    {
        this.contenidoVitaminas = contenidoVitaminas;
    }
    public void setContenidoPorcentaje(int contenidoGrasas,int contenidoHidratos,int contenidoProteinas)
    {
        this.contenidoGrasas = contenidoGrasas;
        this.contenidoHidratos = contenidoHidratos;
        this.contenidoProteinas = contenidoProteinas;
    }
    public void setOrigenAnimal (boolean origenAnimal)
    {
        this.origenAnimal = origenAnimal;
    }

    // Metodos "getter". Permiten obtener los valores de las variables
    public String getNombre()
    {
        return this.nombre;
    }
    public String getDescripcion()
    {
        return this.descripcion;
    }
    public char getContenidoVitaminas()
    {
        return this.contenidoVitaminas;
    }
    public int getContenidoGrasas()
    {
        return this.contenidoGrasas;
    }
    public int getContenidoHidratos()
    {
        return this.contenidoHidratos;
    }
    public int getContenidoProteinas()
    {
        return this.contenidoProteinas;
    }
    public boolean getOrigenAnimal()
    {
        return this.origenAnimal;
    }

}

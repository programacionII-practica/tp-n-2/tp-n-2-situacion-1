package situacion;

public class InfoAlimento { // Metodo encargado de imprimir por pantalla la informacion de Alimento 
    public void mostrarInformacion(Alimento InstanciaAlimento)  // (los valores de sus atributos)
    {
        System.out.println("Nombre del alimento: " + InstanciaAlimento.getNombre());
        System.out.println("Descripcion: " + InstanciaAlimento.getDescripcion());
        System.out.print("\nContenido de grasas del alimento: ");
        if (InstanciaAlimento.getContenidoVitaminas() == 'A')
        {
            System.out.println("Alto");
        }
        else if (InstanciaAlimento.getContenidoVitaminas() == 'M')
        {
            System.out.println("Medio");
        }
        else
        {
            System.out.println("Bajo");
        }
        System.out.println("Contenido de grasas: " + InstanciaAlimento.getContenidoGrasas() + "%");
        System.out.println("Contenido de hidratos: " + InstanciaAlimento.getContenidoHidratos() + "%");
        System.out.println("Contenido de proteinas: " + InstanciaAlimento.getContenidoProteinas() + "%\n");
        if (InstanciaAlimento.getOrigenAnimal() == false)
        {
            System.out.print("No ");
        }
        System.out.println("es de origen animal");
    }
    // Metodo que permite determinar si un alimento especifico es dietetico o no. Retorna un valor booleano
    public boolean esDietetico(Alimento InstanciaAlimento)
    {
        // Si el alimento tiene un contenido en grasas mayor al 20% o contenido en vitaminas "Bajo",
        // se considera no dietetico y se retorna un "falso"
        if (InstanciaAlimento.getContenidoGrasas() > 20 || InstanciaAlimento.getContenidoVitaminas() == 'B')
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // Metodo que determina si un alimento es recomendable para deportistas. Retorna un valor booleano
    public boolean esRecomendableDeportistas(Alimento InstanciaAlimento)
    {
        /*
         Si el alimento cumple con todas las condiciones propuestas
        - Contenido en Grasas entre 10% y 15%
        - Contenido en Hidratos entre 55% y 65%
        - Contenido en Proteinas entre 30% y 35%
         Se considera recomendable para deportistas (retorna true). Caso contrario, no lo es y retorna "false"
        */
        if 
        ((InstanciaAlimento.getContenidoGrasas() >= 10) && (InstanciaAlimento.getContenidoGrasas() <= 15) 
            && (InstanciaAlimento.getContenidoHidratos() >= 55) && (InstanciaAlimento.getContenidoHidratos() <= 65)
            && (InstanciaAlimento.getContenidoProteinas() >= 30) && (InstanciaAlimento.getContenidoProteinas() <= 35)
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Metodo encargado del calculo del Valor energetico. Retorna un valor flotante (numerico decimal)
    public float valorEnergetico(Alimento InstanciaAlimento)
    {
        return (float) InstanciaAlimento.getContenidoGrasas() * (float) 9.4 + (float) InstanciaAlimento.getContenidoHidratos() * (float) 4.1 + (float) InstanciaAlimento.getContenidoProteinas() * (float) 5.3;
    } 
    // Se utiliza "casting" para asegurar que el valor obtenido sea de tipo float.
}

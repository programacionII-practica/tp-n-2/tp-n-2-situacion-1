Se necesita realizar una representación de objetos de tipo alimento. De los mismos se conocen su
descripción, el contenido de lípidos, el contenido de hidratos de carbono y el contenido de proteínas expresado en porcentajes, si es o no de origen animal, el contenido de vitaminas representado por A (Alto), M (medio) y B (Bajo).
Adicionalmente se pide definir otra clase que permita instanciar objetos de la clase alimento y
mostrar la información del alimento, indicar si un alimento es dietético, lo cual sucede cuando el
alimento contiene menos del 20 % de grasas y el contenido de vitamina no en bajo; si es recomendable para deportistas lo cual es cierto si el alimento contiene lo siguiente: proteínas: 10-
15%; hidratos de carbono:55-65% y grasas 30-35%; e informar el valor energético del alimento.
Teniendo en cuenta que este valor resulta de aplicar la siguiente formula:
lípidos * 9,4 +hidratos * 4,1 +proteínas * 5,3.


NOTA: Los archivos de este repositorio fueron transferidos de un repositorio anterior https://gitlab.com/Gustavocon/tp-n-2-situacion-1
